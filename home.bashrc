# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

export VISUAL=$(which vim)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# Keep bash history up-to-date across multiple sessions (tmux, etc.).
export PROMPT_COMMAND='history -a'

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
  # We have color support; assume it's compliant with Ecma-48
  # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
  # a case would tend to support setf rather than setaf.)
  color_prompt=yes
    else
  color_prompt=
    fi
fi

parse_git_branch() {
    git rev-parse --git-dir &> /dev/null
    git_status="$(git status 2> /dev/null)"
    branch_pattern="^On branch ([^${IFS}]*)"
    remote_pattern="Your branch is (ahead|behind)"
    diverge_pattern="Your branch and (.*) have diverged"
    if [[ ! ${git_status} =~ "working directory clean" ]]; then
	state='☼'
    fi
    if [[ ${git_status} =~ ${remote_pattern} ]]; then
	if [[ ${BASH_REMATCH[1]} == "ahead" ]]; then
	    remote='↑'
	else
	    remote='↓'
	fi
    fi
    if [[ ${git_status} =~ ${diverge_pattern} ]]; then
	remote='↕'
    fi
    if [[ ${git_status} =~ ${branch_pattern} ]]; then
	branch=${BASH_REMATCH[1]}
	echo " (${branch})${remote}${state}"
    fi
}

if [ "$color_prompt" = yes ]; then
    PS1='\[\033[30;47m\]\t\[\033[0m\] ${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[0m\]:\[\033[01;34m\]\w\[\033[0m\]\[\033[1;36m\]$(parse_git_branch)\[\033[0m\]» '
else
    PS1='\t ${debian_chroot:+($debian_chroot)}\u@\h:\w$(parse_git_branch)» '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

alias colordiff='sed -e '\''s/^+/\x1b[32m+/;s/^-/\x1b[31m-/;s/^\@/\x1b[33m@/;s/$/\x1b[0m/'\'''
alias l='less -FX'
alias tat='tig --all --topo-order'
alias tst='tig status'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
LESS=-FX
PATH=~/bin:$PATH

# Fix stale DISPLAY variables inside tmux sessions, see <http://yubinkim.com/?p=203>.
if [ -n "$TMUX" ]; then
    for name in $(tmux ls -F '#{session_name}'); do
      tmux setenv -g -t $name DISPLAY $DISPLAY #set display for all sessions
    done
fi

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
