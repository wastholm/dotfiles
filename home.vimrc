filetype on
filetype plugin on
filetype indent on  " File type based indentation.

au BufNewFile,BufRead *.tt set ft=tt2html

set expandtab
set hlsearch
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<
set modeline
set modelines=5
set mouse=a
set relativenumber
set shiftwidth=4
set t_Co=256
set ts=4

" Don't lose selection when fixing indentation (< and >).
vnoremap < <gv
vnoremap > >gv

" Better joining of lines (J).
if v:version > 703 || v:version == 703 && has('patch541')
  set formatoptions+=j
endif

colorscheme delek
hi CursorLine cterm=NONE ctermbg=black

function! NumberToggle()
    if exists('+relativenumber')
        if (&relativenumber == 1)
            set number
            set norelativenumber
        elseif (&number == 1)
            set nonumber
            set norelativenumber
        else
            set nonumber
            set relativenumber
        endif
    else
        if (&number == 1)
            set nonumber
        else
            set number
        endif
    endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>
nnoremap <C-l> :set cursorline!<cr>

function! GitBlame()
    :silent !tig blame "%"
    redraw!
endfunc

function! GitDiff()
    :silent !git diff -- "%" | tig
    redraw!
endfunc

function! GitLog()
    :silent !tig --all --topo-order
    redraw!
endfunc

function! GitStatus()
    :silent !tig status
    redraw!
endfunc

nnoremap gb :call GitBlame()<cr>
nnoremap gd :call GitDiff()<cr>
nnoremap gl :call GitLog()<cr>
nnoremap gs :call GitStatus()<cr>

autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0
